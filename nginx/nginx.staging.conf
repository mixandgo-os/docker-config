upstream railsapp { 
  server app:3000; 
}

server {
  listen 80;

  server_name staging.example.com;

  location / {
    return 301 https://$host$request_uri;
  }
}

# This might be required when you generate a new SSL cert with LetsEncrypt
#server {
#  listen 80;
#
#  root /app/public;
#
#  try_files $uri @railsapp;
#
#  location ^~ /assets/ {
#    gzip_static on;
#    expires max;
#    add_header Cache-Control public;
#  }
#
#  location @railsapp {
#    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
#    proxy_set_header Host $http_host;
#    proxy_redirect off;
#    proxy_pass http://railsapp;
#  }
#
#  error_page 500 502 503 504 /500.html;
#  client_max_body_size 20M;
#  keepalive_timeout 10;
#}

server {
  listen 443 ssl;

  server_name staging.example.com;

  root /app/public;

  ssl_certificate /etc/ssl/certificates/staging.example.com-fullchain.pem;
  ssl_certificate_key /etc/ssl/certificates/staging.example.com-key.pem;

  # from https://cipherli.st/
  # and https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_prefer_server_ciphers on;
  ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
  ssl_ecdh_curve secp384r1;
  ssl_session_cache shared:SSL:10m;
  ssl_session_tickets off;
  ssl_stapling on;
  ssl_stapling_verify on;
  resolver 8.8.8.8 8.8.4.4 valid=300s;
  resolver_timeout 5s;
  # Disable preloading HSTS for now.  You can use the commented out header line that includes
  # the "preload" directive if you understand the implications.
  #add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";
  add_header Strict-Transport-Security "max-age=63072000; includeSubdomains";
  add_header X-Frame-Options DENY;
  add_header X-Content-Type-Options nosniff;

  try_files $uri @railsapp;

  location ^~ /assets/ {
    gzip_static on;
    expires max;
    add_header Cache-Control public;
  }

  location @railsapp {
    auth_basic              "Restricted";
    auth_basic_user_file    /etc/nginx/.htpasswd;

    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_redirect off;
    proxy_pass http://railsapp;
  }

  error_page 500 502 503 504 /500.html;
  client_max_body_size 20M;
  keepalive_timeout 10;
}
